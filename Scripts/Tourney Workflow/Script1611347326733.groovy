import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Login Page/Email'), GlobalVariable.email)

WebUI.setText(findTestObject('Login Page/password'), GlobalVariable.password)

WebUI.click(findTestObject('Login Page/Sign In'))

WebUI.waitForElementVisible(findTestObject('Top Navigation Menu/Dashboard'), 60)

WebUI.enhancedClick(findTestObject('Top Navigation Menu/Tourneys'))

WebUI.enhancedClick(findTestObject('Tourneys page/Tourney2 listed'))

WebUI.enhancedClick(findTestObject('Tourneys page/Assign Staff button'))

WebUI.enhancedClick(findTestObject('Tourneys page/Assign Staff User1 checkbox'))

WebUI.enhancedClick(findTestObject('Tourneys page/Assign Staff SAVE button'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Tourneys page/Assign Recruits button'))

WebUI.enhancedClick(findTestObject('Tourneys page/Select Recruits dropdown'))

WebUI.enhancedClick(findTestObject('Tourneys page/Assign Recruits dropdown list1'))

WebUI.enhancedClick(findTestObject('Tourneys page/Assign Recruits SAVE button'))

WebUI.enhancedClick(findTestObject('Tourneys page/Select Tourney Team dropdown'))

WebUI.enhancedClick(findTestObject('Tourneys page/Select Tourney Team dropdown1'))

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('Tourneys page/Tourney MY RECRUITS GAMES tab'))

WebUI.delay(3)

WebUI.verifyElementPresent(findTestObject('Tourneys page/GAMES tab row1'), 0)

gameDate = WebUI.getText(findTestObject('Tourneys page/GAMES tab row1 Date'))

WebUI.enhancedClick(findTestObject('Top Navigation Menu/Schedule'))

WebUI.enhancedClick(findTestObject('Schedule Page/Calendar from date filter'))

WebUI.sendKeys(findTestObject('Schedule Page/Calendar from date filter'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Schedule Page/Calendar from date filter'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Schedule Page/Calendar from date filter'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Schedule Page/Calendar from date filter'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Schedule Page/Calendar from date filter'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Schedule Page/Calendar from date filter'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Schedule Page/Calendar from date filter'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Schedule Page/Calendar from date filter'), Keys.chord(Keys.BACK_SPACE))

WebUI.sendKeys(findTestObject('Schedule Page/Calendar from date filter'), Keys.chord(Keys.BACK_SPACE))

WebUI.setText(findTestObject('Schedule Page/Calendar from date filter'), gameDate)

WebUI.sendKeys(findTestObject('Schedule Page/Calendar from date filter'), Keys.chord(Keys.ENTER))

WebUI.verifyElementPresent(findTestObject('Schedule Page/Row1 on Schedule List'), 0)

WebUI.enhancedClick(findTestObject('Top Navigation Menu/Tourneys'))

WebUI.enhancedClick(findTestObject('Tourneys page/Tourney2 listed'))

WebUI.enhancedClick(findTestObject('Tourneys page/Edit Assign Staff Pencil'))

WebUI.enhancedClick(findTestObject('Tourneys page/Assign Staff User1 checkbox'))

WebUI.enhancedClick(findTestObject('Tourneys page/Assign Staff SAVE button'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Tourneys page/Delete Attending Recruit1'))

WebUI.enhancedClick(findTestObject('Tourneys page/Confirm Delete Attending Recruit1'))

WebUI.closeBrowser()

