import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setViewPortSize(1440, 841)

WebUI.setText(findTestObject('Login Page/Email'), GlobalVariable.email)

WebUI.setText(findTestObject('Login Page/password'), GlobalVariable.password)

WebUI.click(findTestObject('Login Page/Sign In'))

WebUI.waitForElementVisible(findTestObject('Top Navigation Menu/Dashboard'), 60)

WebUI.enhancedClick(findTestObject('Top Navigation Menu/Dashboard'))

WebUI.delay(3)

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/blank 1-PG on Draft Board (Dashboard)'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/blank 1-PG on Draft Board (Dashboard)'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/Draft Board Dropdown Player1'))

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Recruit Profile (view)/Alerts button'))

WebUI.enhancedClick(findTestObject('Recruit Profile (view)/CLOSE button'))

WebUI.verifyElementVisible(findTestObject('Dashboard Page/Draft Board (Dashboard)/Alerts bell indicator'))

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Recruit Profile (view)/Alerts button'))

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Recruit Profile (view)/CLOSE button'))

WebUI.delay(1)

WebUI.verifyElementNotPresent(findTestObject('Dashboard Page/Draft Board (Dashboard)/Alerts bell indicator'), 0)

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/Delete 1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/Draft Board Season Dropdown'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/Draft Board Season Dropdown - 2023'))

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/blank 1-PG on Draft Board (Dashboard)'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/blank 1-PG on Draft Board (Dashboard)'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/Draft Board Dropdown Player1'))

WebUI.delay(1)

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/blank 2-PG on Draft Board (Dashboard)'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/blank 2-PG on Draft Board (Dashboard)'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/Recruit Search field'))

WebUI.sendKeys(findTestObject('Dashboard Page/Draft Board (Dashboard)/Recruit Search field'), 'Mikey Williams')

WebUI.delay(2)

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/Draft Board Dropdown Player2'))

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/2-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/Delete 2-PG on Draft Board'))

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/Delete 1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Dashboard Page/Draft Board (Dashboard)/Draft Board Link'))

WebUI.mouseOver(findTestObject('Recruits Page/Draft Board (Recruits)/blank PG1 Draft Board-Recruits'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/blank PG1 Draft Board-Recruits'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Draft Board dropdown P1 (Recruits)'))

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Draft Board Color Dropper icon'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Yellow Color Draft Board'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'), 'class', 
    'draft-board-player dashboard-player css-1n2x8eh-DraftBoard', 0)

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Draft Board Color Dropper icon'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Green Color Draft Board'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'), 'class', 
    'draft-board-player dashboard-player css-15mrb88-DraftBoard', 0)

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Draft Board Color Dropper icon'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Draft Board Color pencil icon'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Add Draft Board Color plus button'))

WebUI.doubleClick(findTestObject('Recruits Page/Draft Board (Recruits)/Draft Board Color Hex field'))

WebUI.sendKeys(findTestObject('Recruits Page/Draft Board (Recruits)/Draft Board Color Hex field'), '4832a8')

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Manage Colors Close button'))

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Draft Board Color Dropper icon'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/My Custom Color Draft Board'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'), 'class', 
    'draft-board-player dashboard-player css-ms4n7m-DraftBoard', 0)

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Draft Board Color Dropper icon'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Reset Draft Board color'))

WebUI.delay(1)

WebUI.verifyElementAttributeValue(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'), 'class', 
    'draft-board-player dashboard-player css-g7hzzu-DraftBoard', 0)

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Draft Board Color Dropper icon'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Draft Board Color pencil icon'))

WebUI.mouseOver(findTestObject('Recruits Page/Draft Board (Recruits)/Manage Colors Custom1'))

WebUI.waitForElementPresent(findTestObject('Recruits Page/Draft Board (Recruits)/Delete custom color1'), 0)

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Delete custom color1'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Manage Colors Close button'))

WebUI.mouseOver(findTestObject('Dashboard Page/Draft Board (Dashboard)/1-PG on Draft Board'))

WebUI.enhancedClick(findTestObject('Recruits Page/Draft Board (Recruits)/Delete DraftBoard PG1'))

WebUI.delay(1)

WebUI.closeBrowser()

