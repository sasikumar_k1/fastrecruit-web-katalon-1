import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Login Page/Email'), GlobalVariable.email)

WebUI.setText(findTestObject('Login Page/password'), GlobalVariable.password)

WebUI.click(findTestObject('Login Page/Sign In'))

WebUI.waitForElementVisible(findTestObject('Top Navigation Menu/Dashboard'), 60)

WebUI.click(findTestObject('Top Navigation Menu/Recruits'))

WebUI.click(findTestObject('Recruits Page/ADD RECRUIT button'))

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/LinkToFastScout Folder/Link To FastScout button'))

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/LinkToFastScout Folder/League_Division dropdown'))

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/LinkToFastScout Folder/D-II League'))

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/LinkToFastScout Folder/Team dropdown (LFS)'))

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/LinkToFastScout Folder/Florida Southern (LFS)'))

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/LinkToFastScout Folder/Player dropdown (LFS)'))

WebUI.enhancedClick(findTestObject('Recruits Page/Add New Recruit modal/LinkToFastScout Folder/Player10 (LFS)'))

WebUI.click(findTestObject('Recruits Page/Add New Recruit modal/SAVE button'))

WebUI.setText(findTestObject('Recruits Page/Recruit Search Field on Recruits page'), 'Wes Bongiorni')

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Recruits Page/Archive recruit'))

WebUI.delay(1)

WebUI.click(findTestObject('Recruits Page/All Recruits List selection'))

WebUI.delay(1)

WebUI.click(findTestObject('Recruits Page/Archived List selection'))

WebUI.delay(1)

WebUI.click(findTestObject('Recruits Page/Archived list ellipsis button'))

WebUI.delay(1)

WebUI.click(findTestObject('Recruits Page/Delete1'))

WebUI.delay(1)

WebUI.enhancedClick(findTestObject('Recruits Page/Delete2'))

WebUI.delay(2)

WebUI.closeBrowser()

