import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Login Page/Email'), GlobalVariable.email)

WebUI.setText(findTestObject('Login Page/password'), GlobalVariable.password)

WebUI.click(findTestObject('Login Page/Sign In'))

WebUI.waitForElementVisible(findTestObject('Top Navigation Menu/Dashboard'), 60)

WebUI.enhancedClick(findTestObject('Top Navigation Menu/Recruits'))

WebUI.enhancedClick(findTestObject('Recruits Page/Recruit Row1 Name'))

WebUI.enhancedClick(findTestObject('Recruits Page/Recruit Profile (View)/VIDEOS tab'))

WebUI.uploadFile(findTestObject('Recruit Profile (view)/Video Upload object'), '/Users/jefffeldman/Desktop/Vids/NBA Playoffs 2019.mp4')

WebUI.verifyElementPresent(findTestObject('Recruit Profile (view)/Recruit Video row1'), 0)

WebUI.delay(2)

WebUI.mouseOver(findTestObject('Recruit Profile (view)/Recruit Video row1'))

WebUI.enhancedClick(findTestObject('Recruit Profile (view)/Delete Video Row1'))

WebUI.enhancedClick(findTestObject('Recruit Profile (view)/Confirm Delete Video'))

WebUI.verifyElementNotPresent(findTestObject('Recruit Profile (view)/Recruit Video row1'), 0)

WebUI.enhancedClick(findTestObject('Recruit Profile (view)/CLOSE button'))

WebUI.closeBrowser()

