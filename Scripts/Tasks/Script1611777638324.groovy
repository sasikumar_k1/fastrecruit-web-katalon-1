import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.URL)

WebUI.setText(findTestObject('Login Page/Email'), GlobalVariable.email)

WebUI.setText(findTestObject('Login Page/password'), GlobalVariable.password)

WebUI.click(findTestObject('Login Page/Sign In'))

WebUI.waitForElementVisible(findTestObject('Top Navigation Menu/Dashboard'), 60)

WebUI.enhancedClick(findTestObject('Top Navigation Menu/Recruits'))

WebUI.enhancedClick(findTestObject('Recruits Page/Tasks tab'))

WebUI.enhancedClick(findTestObject('Recruits Page/Tasks/Add Task button'))

WebUI.enhancedClick(findTestObject('Recruits Page/Tasks/Add Task Recruit field'))

WebUI.enhancedClick(findTestObject('Recruits Page/Tasks/Add Task recruit dropdown1'))

WebUI.enhancedClick(findTestObject('Recruits Page/Tasks/Add Task Assigned To field'))

WebUI.enhancedClick(findTestObject('Recruits Page/Tasks/Add Task Assigned To dropdown1'))

WebUI.enhancedClick(findTestObject('Recruits Page/Tasks/Add Task Instruction field'))

WebUI.setText(findTestObject('Recruits Page/Tasks/Add Task Instruction field'), 'test task')

WebUI.enhancedClick(findTestObject('Recruits Page/Tasks/Save new task'))

WebUI.verifyElementVisible(findTestObject('Recruits Page/Tasks/Due task1 row'))

WebUI.mouseOver(findTestObject('Recruits Page/Tasks/Due task1 ellipsis'))

WebUI.enhancedClick(findTestObject('Recruits Page/Tasks/Due task1 ellipsis'))

WebUI.enhancedClick(findTestObject('Recruits Page/Tasks/Mark Tasks Completed button'))

WebUI.verifyElementNotPresent(findTestObject('Recruits Page/Tasks/Due task1 row'), 0)

WebUI.closeBrowser()

