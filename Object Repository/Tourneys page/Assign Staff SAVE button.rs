<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Assign Staff SAVE button</name>
   <tag></tag>
   <elementGuidId>faae3146-a23f-4398-804d-2d38424c7885</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[16]/div/section/div[2]/div/footer/div/button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[16]/div/section/div[2]/div/footer/div/button[2]</value>
   </webElementProperties>
</WebElementEntity>
