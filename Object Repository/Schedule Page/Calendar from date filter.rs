<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Calendar from date filter</name>
   <tag></tag>
   <elementGuidId>f2222d2b-a5d0-4a9e-ac07-1a37f72c5898</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;scheduleDatePicker&quot;]/div/div[1]/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;scheduleDatePicker&quot;]/div/div[1]/div/input</value>
   </webElementProperties>
</WebElementEntity>
