<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Draft Board Season Dropdown</name>
   <tag></tag>
   <elementGuidId>ce38b401-75e1-436f-8f18-3e82b3f84858</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'draftBoardSeasonDropdown']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>draftBoardSeasonDropdown</value>
   </webElementProperties>
</WebElementEntity>
