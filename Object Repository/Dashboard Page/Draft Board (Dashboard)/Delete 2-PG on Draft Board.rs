<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Delete 2-PG on Draft Board</name>
   <tag></tag>
   <elementGuidId>56dbf125-b851-448f-b008-5a9a8fe43d16</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'draft-delete-PG-1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>draft-delete-PG-1</value>
   </webElementProperties>
</WebElementEntity>
