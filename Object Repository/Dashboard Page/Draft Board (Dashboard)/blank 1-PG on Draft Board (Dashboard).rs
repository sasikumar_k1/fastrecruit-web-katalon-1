<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>1_PG cell on Draft Board on Dashboard</description>
   <name>blank 1-PG on Draft Board (Dashboard)</name>
   <tag></tag>
   <elementGuidId>1d5ad8e5-2093-4ac5-9e3a-59f5a0d50409</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class, 'drftBoarddb-PGJPG')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>drftBoarddb-PGJPG</value>
   </webElementProperties>
</WebElementEntity>
