<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Recruiters checkbox</name>
   <tag></tag>
   <elementGuidId>1cbece5d-3da3-42cf-829e-3070efb00ee2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[4]/div/section/div[2]/div/section/div/div[5]/div/div[2]/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[4]/div/section/div[2]/div/section/div/div[5]/div/div[2]/div/label</value>
   </webElementProperties>
</WebElementEntity>
