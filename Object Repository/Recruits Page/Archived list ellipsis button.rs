<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Archived list ellipsis button</name>
   <tag></tag>
   <elementGuidId>3764d0f1-69c2-4548-84d4-cde9fabdbe34</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='appContainer']/div/div[3]/div/div/section/div/div[2]/div/div/table/tbody/tr/td[10]/div/span/span/i</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'fa fa-ellipsis-h']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>i.fa.fa-ellipsis-h</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-ellipsis-h</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appContainer&quot;)/div[@class=&quot;flex-grow-1 display-flex flex-direction-column&quot;]/div[3]/div[1]/div[1]/section[@class=&quot;recruit-table-container&quot;]/div[@class=&quot;recruit-list-table display-flex flex-direction-column&quot;]/div[@class=&quot;recruitlist-tbody&quot;]/div[1]/div[1]/table[1]/tbody[1]/tr[@class=&quot;align-items-center no-border-top&quot;]/td[10]/div[1]/span[@class=&quot;dropdown&quot;]/span[@class=&quot;color-primary hover-color-primary-darken lg-font&quot;]/i[@class=&quot;fa fa-ellipsis-h&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appContainer']/div/div[3]/div/div/section/div/div[2]/div/div/table/tbody/tr/td[10]/div/span/span/i</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/span/span/i</value>
   </webElementXpaths>
</WebElementEntity>
