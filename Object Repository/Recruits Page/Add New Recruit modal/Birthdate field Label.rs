<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Birthdate field Label</name>
   <tag></tag>
   <elementGuidId>fa375462-b846-4f65-a9e4-16253f8214a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;recruit-info&quot;]/div/section/div[4]/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;recruit-info&quot;]/div/section/div[4]/label</value>
   </webElementProperties>
</WebElementEntity>
