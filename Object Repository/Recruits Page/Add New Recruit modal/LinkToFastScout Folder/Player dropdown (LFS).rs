<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Player dropdown (LFS)</name>
   <tag></tag>
   <elementGuidId>ecf848b0-5662-481d-a8b7-90f956597368</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Player' or . = 'Player') and contains(@class, 'playerDropDown')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Text</name>
      <type>Main</type>
      <value>Player</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>playerDropDown</value>
   </webElementProperties>
</WebElementEntity>
