<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Florida Southern (LFS)</name>
   <tag></tag>
   <elementGuidId>4742f27f-22b6-4631-922d-6da34afbf5ec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@aria-label = 'Florida Southern']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Florida Southern</value>
   </webElementProperties>
</WebElementEntity>
