<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Draft Board Color Hex field</name>
   <tag></tag>
   <elementGuidId>d1a242e1-9fb8-43c1-83b9-a74e04e8cda7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[20]/div/section/div[2]/div/section/div[3]/div[2]/div/div[2]/div[2]/div[1]/div/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[20]/div/section/div[2]/div/section/div[3]/div[2]/div/div[2]/div[2]/div[1]/div/div/input</value>
   </webElementProperties>
</WebElementEntity>
