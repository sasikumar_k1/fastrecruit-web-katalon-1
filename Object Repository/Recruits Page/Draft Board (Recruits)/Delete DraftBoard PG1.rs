<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Delete DraftBoard PG1</name>
   <tag></tag>
   <elementGuidId>d0ab793f-dbba-44c1-a4c9-5c61a0efe6ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;draft-delete-PG-0&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;draft-delete-PG-0&quot;]</value>
   </webElementProperties>
</WebElementEntity>
