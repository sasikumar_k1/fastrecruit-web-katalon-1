<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Draft Board Color Dropper icon</name>
   <tag></tag>
   <elementGuidId>f3609f7c-4b30-4784-afbb-5eeaee2fb06e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;draft-color-PG-0&quot;]/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;draft-color-PG-0&quot;]/i</value>
   </webElementProperties>
</WebElementEntity>
