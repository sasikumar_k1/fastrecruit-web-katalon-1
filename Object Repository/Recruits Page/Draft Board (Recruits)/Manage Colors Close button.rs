<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Manage Colors Close button</name>
   <tag></tag>
   <elementGuidId>aa621642-06a0-46b5-9352-0a6c238cf56a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html/body/div[20]/div/section/div[2]/div/section/div[1]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html/body/div[20]/div/section/div[2]/div/section/div[1]/span</value>
   </webElementProperties>
</WebElementEntity>
