<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Reset Draft Board color</name>
   <tag></tag>
   <elementGuidId>bac83bfe-da4d-4dda-9946-cdad727bdd03</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'fa fa-ban mg-right-sm cursor-pointer']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fa fa-ban mg-right-sm cursor-pointer</value>
   </webElementProperties>
</WebElementEntity>
