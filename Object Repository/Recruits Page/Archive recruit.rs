<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Archive recruit</name>
   <tag></tag>
   <elementGuidId>9f3bf6d8-02e7-4da7-8c7e-904934a38bd0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@title = 'Archive']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.color-primary.hover-color-primary-darken.lg-font > svg > path</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;appContainer&quot;]/div/div[3]/div/div/section/div/div[2]/div/div/table/tbody/tr/td[11]/div/span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Archive</value>
   </webElementProperties>
</WebElementEntity>
