<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>All Prospects List selection</name>
   <tag></tag>
   <elementGuidId>f7c71bbd-b9d4-43ef-982d-40bacc640fc4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;seasonDropdown-All Prospects&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;seasonDropdown-All Prospects&quot;]</value>
   </webElementProperties>
</WebElementEntity>
