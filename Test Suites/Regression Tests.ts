<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Regression Tests</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6b2556aa-5803-42e3-8f4b-7a23b2990930</testSuiteGuid>
   <testCaseLink>
      <guid>93005e94-d4ab-4a8d-9503-733ab497b565</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Board</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3758c830-4305-4845-a033-21606f947fa9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Depth Chart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96d12ffb-3055-4b46-849a-b1d1fbf23d62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Dashboard Calendar</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2a34eeb-ec4b-45f3-b7e5-0e66e7e3491b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Add_Archive_Delete New Recruit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1964630-f9fa-4d27-afe7-a0c552e0f8f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tasks</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c049c33f-ef37-437c-ab19-f2d8457be7cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/Link To FastScout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>500ea185-c380-4e04-a6f6-22e17ac7b90d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/Video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>083dffc3-fea9-4b1c-8c07-a9c45b0d3ff3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/FastConnect</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c39411f6-9ac4-44f0-b94d-6d0bc54d7541</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Tourney Workflow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1af8397-80f6-41e5-b53f-829ecb17e688</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Features/Prospects View</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
